import unittest

from src import add


class TestAdd(unittest.TestCase):

    def test_add_two_ints(self):
        self.assertEqual(
            add.add_two_ints(1, 1),
            2
        )
